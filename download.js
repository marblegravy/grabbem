/* download.js */
/* Child component to run the actual download part of the process. */
// Dependencies
var fs = require('fs'),
    url = require('url'),
    http = require('http'),
    exec = require('child_process').exec,
    spawn = require('child_process').spawn,
    assert = require('better-assert'),
    Queue = require("child-queue");
    //should = require('should'),
    //csv = require('csv');
    //parse = require('csv-parse');
    validUrl = require('valid-url'),
    csv = require('fast-csv');

module.exports = function(input, callback) {
   console.log("Processing download: " + input.file_url + " TO " + input.targetDir);
  if (input.file_url && input.targetDir) {
    var options = {
        host: url.parse(file_url).host,
        port: 80,
        path: url.parse(file_url).pathname
    };

    var file_name = url.parse(file_url).pathname.split('/').pop();
    var file = fs.createWriteStream(dir + "/" + file_name);

    http.get(options, function(res) {
        res.on('data', function(data) {
            file.write(data);
        }).on('end', function() {
            file.end();
            console.log(file_name + ' downloaded to ' + dir);
            callback(null, 'success');
        });
    }).on('error', function(e) {
            callback(e);
        });
    }
};