Grabbem
=======

Grabbem is a custom script to download a bunch of files from a csv.

index.js is a generic script that should work for simple files.
docs_dump.js is a script specifically set up to download files from a particular flavour of CSV.

# Usage

Just run:

node docs_dump.js [filename]

It'll download all the files in to the current directory and put everything in a
folder with the current datestamp.

# Disclaimer

This is not a polished script by any means and has been done quickly to get the job
done. There will be bugs.

If you need to fix anything, you're probably best to try and debug it yourself!
