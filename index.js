// Dependencies
var fs = require('fs'),
    url = require('url'),
    http = require('http'),
    exec = require('child_process').exec,
    spawn = require('child_process').spawn,
    assert = require('better-assert'),
    //should = require('should'),
    //csv = require('csv');
    //parse = require('csv-parse');
    validUrl = require('valid-url'),
    csv = require('fast-csv');


// App variables
var DOWNLOAD_DIR = __dirname+'/'+Date.now() + '/',
// Get the csv source from the command line args
    csv_source = process.argv[2]? process.argv[2] : false,
    //track errors - don't quit on error
    error_log = [];

function init(){

  // Check for csv or revert to a test version.
  if (!csv_source) {
      //console.log('FAIL - No csv supplied. Usage : node index.js myfile.csv');
      console.log('WARN - No csv supplied. \n     Expected Usage : node index.js myfile.csv \n     Using demo file (demo.csv) ...');
      csv_source = __dirname+'/demo.csv';
  }

  // We will be downloading the files to a directory, so make sure it's there
  var mkdir = 'mkdir -p ' + DOWNLOAD_DIR;
  var child = exec(mkdir, function(err, stdout, stderr) {
      if (err) throw err;
  });

  // Set up and start processing the csv
  var stream = fs.createReadStream(csv_source);

  csv
    .fromStream(stream, {headers:true})

    .validate(function(data){
        return testForValidData(data);
    })

    .on("data-invalid", function(data){
        console.log('SKIPPING - Uh-oh. There was a problem. Check the error log and try again.');
    })

    .on("data", function(data){
      download_file_httpget(data.url, makeDir(data.nid));
    })
    .on("end", function(){
       console.log("done processing csv");
       if (error_log.length > 0){
        console.log('Errors were encountered:\n',error_log);
      }
    });
}

/* Makes the directory and returns the path to that new directory as a string */
function makeDir(dirName){
    // If we are categorising the files in to categories or labels, create those folders
    var newDir = DOWNLOAD_DIR + dirName;
        mkdir = 'mkdir -p ' + newDir,
        child = exec(mkdir, function(err, stdout, stderr) {
            if (err) throw err;
        });
    return newDir;
}

// Function to download file using HTTP.get and put it in the right folder.
var download_file_httpget = function(file_url, dir) {
    console.log("DOWNLOADING TO: " + dir);
    var options = {
        host: url.parse(file_url).host,
        port: 80,
        path: url.parse(file_url).pathname
    };

    var file_name = url.parse(file_url).pathname.split('/').pop();
    var file = fs.createWriteStream(dir + "/" + file_name);

    http.get(options, function(res) {
        res.on('data', function(data) {
            file.write(data);
        }).on('end', function() {
            file.end();
            console.log(file_name + ' downloaded to ' + dir);
        });
    });
};

function testForValidData(data){
  var testPassed = true;
  if (data.nid){
    var reg = new RegExp('^[0-9]+$');
    if (!reg.test(data.nid)) {
      testPassed = logError(data, "nid value was not numbers only. Found "+data.nid);
    }
  } else {
    testPassed = logError(data, "No column called 'nid' found.");
  }

  if (data.url){
    console.log(validUrl.isUri(data.url));
    if (!validUrl.isUri(data.url)) {
      testPassed = logError(data, "URL was not valid', found "+data.url);
    }
  } else {
    testPassed = logError(data, "No column called 'url' found.");
  }
  return testPassed;
}

function logError(data, errorMsg){
  error_log.push({
    'data':data,
    'message':errorMsg
  });
  return false;
}

// Kick this off
init();