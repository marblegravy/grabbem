// Dependencies
var fs = require('fs'),
    exec = require('child_process').exec,
    assert = require('better-assert'),
    DownloadProgress = require('download-progress'),
    validUrl = require('valid-url'),
    csv = require('fast-csv');


// App variables
var DOWNLOAD_DIR = __dirname+'/'+Date.now() + '/',
// Get the csv source from the command line args
    csv_source = process.argv[2]? process.argv[2] : false,
    //processed list of links
    urls = [];
    //track errors - don't quit on error
    error_log = [];

function init(){
  // Check for csv or revert to a test version.
  if (!csv_source) {
      //console.log('FAIL - No csv supplied. Usage : node index.js myfile.csv');
      console.log('WARN - No csv supplied. \n     Expected Usage : node index.js myfile.csv \n     Using demo file (demo.csv) ...');
      csv_source = __dirname+'/ECC_stream_export_short.csv';
  }

  // We will be downloading the files to a directory, so make sure it's there
  var mkdir = 'mkdir -p ' + DOWNLOAD_DIR;
  var child = exec(mkdir, function(err, stdout, stderr) {
      if (err) throw err;
  });

  // Set up and start processing the csv
  var stream = fs.createReadStream(csv_source);

  csv
    .fromStream(stream, {headers:true})

    .validate(function(data){

        return (data['Documents'].length !== 0);
        //return testForValidData(data);
    })

    .on("data-invalid", function(data){
        console.log('SKIPPING - Uh-oh. There was a problem. Check the error log.');
    })

    .on("data", function(data){
      var nid = data['URL'].split('/').pop();
      var docs = data['Documents'].split(', ');
      for (doc in docs) {
        var filename = docs[doc].split('/').pop();
        queueForDownload({'url': docs[doc], 'dest' : makeDir(nid)+filename});
      };
    })
    .on("end", function(){
       console.log("done processing csv - now starting download process");
       downloadTheThings();
    });
}

/* Makes the directory and returns the path to that new directory as a string */
function makeDir(dirName){
    // If we are categorising the files in to categories or labels, create those folders
    var newDir = DOWNLOAD_DIR + dirName + "/";
        mkdir = 'mkdir -p ' + newDir,
        child = exec(mkdir, function(err, stdout, stderr) {
            if (err) throw err;
        });
        console.log(newDir);
    return newDir;
}

/* Loop through the downloadQueue and download each item consecutively.
  This can probably be modded in to a threaded option later */
function queueForDownload(obj){
  urls.push(obj);
}

/* Actually set the download going */
function downloadTheThings(){
  var options = {};
  var download = DownloadProgress(urls, options);

  download.get(function (err) {
      if (err) { throw new Error(err); }
      console.log('DONE');
      if (error_log.length > 0){
         console.log('Errors were encountered:\n',error_log);
      }
  });
}

function testForValidData(data){
  var testPassed = true;
  if (data.nid){
    var reg = new RegExp('^[0-9]+$');
    if (!reg.test(data.nid)) {
      testPassed = logError(data, "nid value was not numbers only. Found "+data.nid);
    }
  } else {
    testPassed = logError(data, "No column called 'nid' found.");
  }

  if (data.documents){
    console.log(validUrl.isUri(data.url));
    if (!validUrl.isUri(data.url)) {
      testPassed = logError(data, "URL was not valid', found "+data.url);
    }
  } else {
    testPassed = logError(data, "No column called 'url' found.");
  }
  return testPassed;
}

function logError(data, errorMsg){
  error_log.push({
    'data':data,
    'message':errorMsg
  });
  return false;
}

// Kick this off
init();